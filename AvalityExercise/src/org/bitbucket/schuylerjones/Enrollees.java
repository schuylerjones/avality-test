package org.bitbucket.schuylerjones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class Enrollees implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userId;
	private String firstName;
	private String lastName;
	private Integer version;
	private String insuranceCompany;

	public Enrollees() {
	}

	public Enrollees(String userId, String firstName, String lastName, Integer version, String insuranceCompany) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.version = version;
		this.insuranceCompany = insuranceCompany;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	static class SortbyId implements Comparator<Enrollees> {

		@Override
		public int compare(Enrollees o1, Enrollees o2) {

			return Integer.parseInt(o1.getUserId()) - Integer.parseInt(o2.getUserId());
		}
	}

	static class EnrolleeMaxVersionNumber implements Comparator<Enrollees> {

		@Override
		public int compare(Enrollees e1, Enrollees e2) {
			return e1.getVersion().compareTo(e2.getVersion());
		}

	}

	static class SortbyName implements Comparator<Enrollees> {

		@Override
		public int compare(Enrollees o1, Enrollees o2) {

			int lastNameCompare = o1.getLastName().compareTo(o2.getLastName());
			if (lastNameCompare != 0)
				return lastNameCompare;
			return o1.getFirstName().compareTo(o2.getFirstName());

		}

	}

	static class SortByInsuranceCompany implements Comparator<Enrollees> {

		@Override
		public int compare(Enrollees e1, Enrollees e2) {

			// for comparison
			int InusranceCompanyCompare = e1.getInsuranceCompany().compareTo(e2.getInsuranceCompany());

			// 2-level comparison using if-else block
			if (InusranceCompanyCompare == 0) {
				return InusranceCompanyCompare;
			} else if (InusranceCompanyCompare > 0) {
				return InusranceCompanyCompare;
			} else {
				return InusranceCompanyCompare;
			}

		}
	}

	public void writeFile(Map<String, List<Enrollees>> m) {

		List<Enrollees> list = new ArrayList<Enrollees>();
		FileOutputStream out = null;
		File file = null;
		String path;
		ObjectOutputStream objectOut = null;
		Map<String, Integer> duplicateIdMap = null;

		// for (Map.Entry<String,List<Enrollees>> entry : m.entrySet()) {
		// System.out.println("Key = " + entry.getKey() +
		// ", Value = " + entry.getValue());

		Set<String> keySet = m.keySet();
		for (String key : keySet) {
			list = m.get(key.replaceAll("\\s+", ""));
			Collections.sort(list, new SortbyName());
		}

		for (String key : keySet) {
			list = m.get(key.replaceAll("\\s+", ""));
			// System.out.println("List list:" + list);

			if (list.size() <= 1) {
				path = ".." + File.separator + "AvalityExercise" + File.pathSeparator + "resources" + File.pathSeparator
						+ list.get(0).getInsuranceCompany() + ".csv";
				// System.out.println(path);

				try {
					file = new File(path);
					if (!file.exists()) {
						file.createNewFile();
					}

					out = new FileOutputStream(path);
					objectOut = new ObjectOutputStream(out);
					objectOut.writeObject(list.get(0));
					objectOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (objectOut != null) {
							objectOut.close();
						}

					} catch (IOException e1) {

						e1.printStackTrace();
					}

				}

			} else {

				duplicateIdMap = new HashMap<>();
				for (Enrollees e : list) {
					Integer count = duplicateIdMap.get(e.getUserId());
					if (count == null) {
						duplicateIdMap.put(e.getUserId(), 1);
					} else {
						duplicateIdMap.put(e.getUserId(), ++count);
					}
				}

				boolean isduplicate = false;
				Enrollees maxVersion = new Enrollees();
				Set<Entry<String, Integer>> entrySet = duplicateIdMap.entrySet();
				for (Entry<String, Integer> entry : entrySet) {
					if (entry.getValue() > 1) {

						isduplicate = true;
						maxVersion = Collections.max(list, new EnrolleeMaxVersionNumber());
						System.out.println(maxVersion);

						path = ".." + File.separator + "AvalityExercise" + File.pathSeparator + "resources"
								+ File.pathSeparator + list.get(0).getInsuranceCompany() + ".csv";
						System.out.println(path);

						try {
							file = new File(path);
							if (!file.exists()) {
								file.createNewFile();
							}

							out = new FileOutputStream(path);
							objectOut = new ObjectOutputStream(out);
							objectOut.writeObject(maxVersion);
							objectOut.close();
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								if (objectOut != null) {
									objectOut.close();
								}

							} catch (IOException e1) {

								e1.printStackTrace();
							}

						}

					}

					if (!isduplicate) {
						path = ".." + File.separator + "AvalityExercise" + File.pathSeparator + "resources"
								+ File.pathSeparator + list.get(0).getInsuranceCompany() + ".csv";
						// System.out.println(path);

						try {
							file = new File(path);
							if (!file.exists()) {
								file.createNewFile();
							}
							out = new FileOutputStream(path);

							for (int i = 0; i < list.size() - 1; i++) {

								objectOut = new ObjectOutputStream(out);
								objectOut.writeObject(list.get(i));
							}

						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								if (objectOut != null) {
									objectOut.close();
								}

							} catch (IOException e1) {

								e1.printStackTrace();
							}
						}

					}
				}

			}

		}

	}

	// System.out.println("after" + list);

	@Override
	public String toString() {
		return "Enrollees [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", version="
				+ version + ", insuranceCompany=" + insuranceCompany + "]";
	}

	public void readFile() {

		String csvFile = "../AvalityExercise/resources/C2ImportUsersSample.csv";
		File file = new File(csvFile);

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		Enrollees en = null;
		List<Enrollees> enlist = null;
		Map<String, List<Enrollees>> map = new HashMap<String, List<Enrollees>>();

		try {

			br = new BufferedReader(new FileReader(csvFile));

			br.readLine();
			String key;
			while ((line = br.readLine()) != null) {
				String[] e = line.split(cvsSplitBy);

				key = e[4].replaceAll("\\s+", "");
				if (e.length > 0) {
					en = new Enrollees(e[0], e[1], e[2], Integer.parseInt(e[3]), key);

					if (map.containsKey(key)) {

						enlist = new ArrayList<Enrollees>();
						enlist = map.get(key);
						enlist.add(en);

					}

					else {
						enlist = new ArrayList<Enrollees>();
						enlist.add(en);
						map.put(key, enlist);
					}

				}

			}

			writeFile(map);

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException ie) {
				ie.printStackTrace();
			}
		}
	}
	
	public static void main(String args[]){
		Enrollees e = new Enrollees();
		e.readFile();
	}

}

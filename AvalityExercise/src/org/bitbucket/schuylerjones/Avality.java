package org.bitbucket.schuylerjones;

import java.util.Stack;

public class Avality {

	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';

	public boolean isValid(String s) {

		Stack<Character> stack = new Stack<Character>();

		try {

			for (int i = 0; i < s.length(); i++) {
				char curr = s.charAt(i);

				if (curr == '(') {
					stack.push(curr);
				}
				if (curr == ')') {
					stack.pop();
				}
			}

			if (stack.empty()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}

}

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
// import registerServiceWorker from './registerServiceWorker';
import RegisterForm from './registrationForm/RegistrationForm';


ReactDOM.render(
  <RegisterForm />,
  document.getElementById('root')
);

// import React from 'react';
// import ReactDOM from 'react-dom';
// import registerServiceWorker from './registerServiceWorker';
// import RegisterForm from './registrationForm/RegisterForm';


// ReactDOM.render(<RegisterForm /> , document.getElementById('root'));

// registerServiceWorker();